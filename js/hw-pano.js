"use strict";
function Hw_Pano (options){
	let object = this;
	let global = {};
	if (!options){options={};}
	
	global.pano={}; // объект с самим пано
	global.click = {}; // объектс с кординатами нажатий
	global.allTarget = []; // Массив с позициями для пинов на сфере ( панораме )
	global.delSeg = []; // Сегменты сферы для удаления ( при переключениями между панорамами )
	global.urlAllPano = ''; // Путь до папки со всеми панорамами
	global.panoName = ''; // текущее имя панорамыы
	global.dir = ''; // полный путь до текущей панорамы
	global.TargetFcns = {}; // функии которые по ключу отработают при нажатии на объект.
	
	//Значения по умолчанию
	global.zoomFunct = options.zoomFunct || function(){};
	global.selector = options.selector || '#pano'; // Селектор блока куда отрисуется панорама
	global.control = {};// объект с контроллерами
	global.control.zoomMax = 4.0; // Максимальное увелечение 
	global.control.zoomMin = 0.5; // Максимальное уменьшение
	global.control.zoomTouchKof = 0.002;// коофицент скорости увелечения зума от пальцев
	global.control.zoomMousKof = 0.0005;// коофицент скорости увелечения зума от мышки
	global.control.rotationSpeed = 0.001; // скорость вращения панорамы
	global.control.yMax = 1.5; // максимальный угол наклона веерх и низ
	global.control.cameraX = 0; // Позиция камеры при смене панорам  X
	global.control.cameraY = 0; // Позиция камеры при смене панорам  Y
		
		
	// Запустит инициализацию, и рендер панорамы
	this.Init = function(){
		InitThree();
		Animate();
	}
	
	// Установит указанную панораму с её пинами.
	this.Set = function(panoName){
		global.panoName = panoName;
		setFullUrlDir();
		THREE.Cache.clear();
		
		object.ClearPins();
		object.SetPano(panoName,()=>{
			object.SetPins(panoName);
			PositionTarget();
		});
	}
	
	// Установит Панораму. ( только картинки не пины )
	this.SetPano = function(panoName,fcn=()=>{}){
		global.panoName = panoName;
		setFullUrlDir();
		let url  = global.dir+'/pano/pano.json';
		hw_ajax({url:url,fc:info => {
				CreatePano(info,fcn());
			}
		,convert:'JSON',err:errCreatePano});
		THREE.Cache.clear();		
	}
	
	// Установит Таргеты ( пины ) на панораму
	this.SetPins = function(panoName,fcn=()=>{}){
		global.panoName = panoName;
		setFullUrlDir();
		THREE.Cache.clear();
		let url  = global.dir+'/target/target.json';
		hw_ajax({url:url,fc:info => {
			fcn();
			CreatePins(info);
		},convert:'JSON'});		
	}
	
	// Очистит панораму
	this.ClearPano = function(){
		THREE.Cache.clear();
		if (global.geometry.allSegment){
			let ii=0;
			while (ii <= global.geometry.allSegment.length) {
				global.pano.scene.remove( global.geometry.allSegment[ii] );
			ii++;
			}
		}
		if (global.delSeg){
			let ii=0;
			while (ii <= global.delSeg.length) {
				global.pano.scene.remove( global.delSeg[ii] );
			ii++;
			}
		}
	}
	
	// Очистит Пины
	this.ClearPins = function(){
		global.allTarget = false;
		global.pano.panoTargetList.innerHTML = "";
		PositionTarget();
	}
	// Раскрасим пано в случайные цвета
	this.RandomColorPano = function(){
		errCreatePano()
	}
	
	/* SETTERS */
	this.SetUrl = function(url=''){ global.urlAllPano = url || ''; } // Установим путь до всех панорам
	this.SetSelector = function(selector=''){ global.selector = selector || '#pano'; } // Установим блок в который рисовать панораму
	this.SetFcn = function(name,fcn){ global.TargetFcns[name] = fcn; } // Установит функцию, котороя привязана на пин.
	this.SetCameraPos = function(x,y){ global.pano.camera.rotation.y = y; global.pano.camera.rotation.x = x} // Установит камеру в новую позицию
	this.SetZoomFcn = (fcn=()=>{})=>{global.zoomFunct = fcn}
	// Установит увеличение камеры
	this.SetZoom = function(sz){
		if (sz >= global.control.zoomMax){sz=global.control.zoomMax;} if (sz <= global.control.zoomMin){sz=global.control.zoomMin;}
		global.pano.camera.zoom = sz;
		global.pano.camera.updateProjectionMatrix();
		global.zoomFunct(sz);
	};
	
	
	/* GETTERS */
	this.GetCameraPos = function(){ return {x:global.pano.camera.rotation.x,y:global.pano.camera.rotation.y}} // Полуит текущею позицию камеры
	this.GetZoom = function(){ return global.pano.camera.zoom} // Вернёт текущий зуум пано

	// Вернт текущею позицию от центра экрана в виде вектора
	this.GetUnScreenPosition = function (){
		let vector = new THREE.Vector3(0,0,0.9);   
		vector.unproject(global.pano.camera);
		return vector;
	};
	// Вернёт позицию в пикселях относительно экрана по вектору камеры ( карочи центр канвас блока ) актуалны только  X и Y
	this.GetScreenPosition = function (){
		let v = object.GetUnScreenPosition();
		let camera =  global.pano.camera;
		let width2 = global.canvas.width2;
		let height2 = global.canvas.height2;
		
		let x = v.x || 0, y = v.y || 0, z = v.z || 0;
		let vector = new THREE.Vector3(x,y,z);
		vector.project(camera);
		vector.x = ( vector.x * width2 ) + width2;
		vector.y = - ( vector.y * height2 ) + height2;
		return { 
			x: vector.x,
			y: vector.y,
			z: vector.z
		}
	};
	
	
	// Установим полный правильный, путь до панорамы
	function setFullUrlDir(){
		if(global.urlAllPano){global.dir = global.urlAllPano+'/'+global.panoName;}
		else {global.dir = global.panoName}
	}

	// Инициализация всех окон блоков и объектов
	function InitThree (){
		global.doc = document;
		global.canvas = {};
		let panoBlock = global.doc.querySelector(global.selector);
		if(panoBlock){
				panoBlock.style.overflow = "hidden";
				panoBlock.style.position = "relative";
				global.pano.panoBlock = panoBlock;
			let panoTargetList = global.doc.createElement('div'); // блок с таргетами
				panoTargetList.classList.add("Hw-three-pano-target-list");
				panoTargetList.style.position = "absolute";
				panoTargetList.style.top = "0";
				panoTargetList.style.left = "0";
				panoTargetList.style.height = "0";
				panoTargetList.style.width = "0";
				panoTargetList.style.zIndex = "10";
				global.pano.panoTargetList = panoTargetList;
				// сама панарама	
			let canvas = global.doc.createElement('canvas'); 
				canvas.classList.add("Hw-three-pano");
				canvas.style.position = "absolute";
				canvas.style.top = "0";
				canvas.style.left = "0";
				canvas.style.height = "100%";
				canvas.style.width = "100%";
				canvas.style.zIndex = "0";
				hw_disable_touch(canvas);
				global.canvasBlock = canvas;
				
			panoBlock.appendChild(global.pano.panoTargetList);
			panoBlock.appendChild(global.canvasBlock);
			
			global.canvas.width = canvas.clientWidth; 
			global.canvas.height = canvas.clientHeight;
			global.canvas.width2 = canvas.clientWidth/2;
			global.canvas.height2 = canvas.clientHeight/2;
			
			let fov = 75, aspect = global.canvas.width/global.canvas.height, near = 0.1, far = 2000;
				
			global.pano.scene = new THREE.Scene(); // создания сцены
			global.pano.camera = new THREE.PerspectiveCamera(fov,aspect,near,far); // создание камеры.
				global.pano.camera.position.set(0, 0, 0); // установим камеру в определенную позицию
				global.pano.camera.rotation.order = "YZX";
				
			global.pano.renderer = new THREE.WebGLRenderer({canvas:global.canvasBlock});
				global.pano.renderer.setSize(global.canvas.width,global.canvas.height)
				global.pano.renderer.clearColor();	
				
			global.canvasBlock.addEventListener('mousedown',CanvasClick);
			global.canvasBlock.addEventListener('touchstart',CanvasClick);
			global.canvasBlock.addEventListener("wheel",CameraWheel);
			THREE.Cache.clear();
		} else { console.log('No selector, or not html object target')}
	}
	// Создание сферы.
	function CteateSphere(Options={}){ //  создаст сферу по заданным параметрам
		THREE.Cache.clear();
		if (!global.geometry) (global.geometry={});
		global.geometry.type = "sphere";
		global.delSeg = global.delSeg.concat(global.geometry.allSegment);
		global.geometry.allSegment = [];
		
		let AllLine = Options.line || 4; // количество линий
		let SegmentLine = Options.segment || 8; // количество сегментов в 1й линии
		let vertex = Options.vertex || 16; // количество точкер в 1м сегменте
		let radius = Options.radius || 5; // радиус сферы
		let scale = Options.scale || [-1,1,1]; // размерность сферы
		let color = Options.color || [0,0,0]; // цвет всей сферы
		let cbFunct = Options.cb || function(){};
		
		let pish = (Math.PI*2); // 1 полный оборот ( горизонтальный )
		let shsi = pish/SegmentLine; // Размер сегмента по горизонтали ( право лево)
		
		let pisv = Math.PI// 1 полный оборот ( вертикальный )
		let svsi = pisv/AllLine; //  Размер сегмента по вертикали ( верх низ)
		let SI = 0;
		for (let il = 0; il < AllLine; il++){	
			let posL = (pisv*il)/AllLine; 
			for (let is = 0;is < SegmentLine;is++){
				let posS = (pish*is)/SegmentLine; // позиция сегмента
				let img = 0, texture = 0,r=0,g=0,b=0;

				if (color == 'random') {r=parseInt(Math.random()*100) , g=parseInt(Math.random()*100) , b=parseInt(Math.random()*100);}
				else {r=(color[0]); g=(color[1]); b=(color[2]); }
				texture = new THREE.MeshBasicMaterial( { color: "rgb(0%, 0%, 0%)",opacity:0,transparent:true });

				let geometry = new THREE.SphereGeometry( radius, vertex, vertex, posS, shsi, posL, svsi); // создадим геометрию
					geometry.scale( scale[0], scale[1], scale[2] ); // инвертнем геометрию
				let obgect = new THREE.Mesh(geometry,texture); // соберем все в 1 объект
				global.geometry.allSegment.push(obgect);
				global.pano.scene.add( obgect );
				SI++;
				if (SI == AllLine*SegmentLine){cbFunct(SI);}
			}
		}
	}
	// Создание панорамы
	function CreatePano(info,fcn=()=>{}){
		if (info.type == "sphere"){
			global.pano.camera.rotation.y = info.cameraY || 0;
			global.pano.camera.rotation.x = info.cameraX || 0;
			
			let line = info.line || 4;
			let seg = info.segment || 8;
			let vertex = info.vertex || 16;
			CteateSphere({
				line:line, // количество линий
				segment:seg, // количество сегментов в 1й линии
				vertex:vertex, // количество точкер в 1м сегменте
				cb:numb => { // функция после завершения цикла сосздания сферы
					SetImg(numb);
					fcn();
				}
			});
		}
	}
	// Если панорама не загрузилась
	function errCreatePano(){
		let line = 4;
		let seg = 8;
		let vertex = 16;
		CteateSphere({
			line:line, // количество линий
			segment:seg, // количество сегментов в 1й линии
			vertex:vertex, // количество точкер в 1м сегменте
			cb:errSetImg // функция после завершения цикла сосздания сферы
		});
	}
	// Установит картиннки в нужные ячейки панорамы
	function SetImg(numb){
		THREE.Cache.clear();
		let allImg = [];
		let ii = 1;
		while (ii <= numb) {
			allImg.push(global.dir+'/pano/'+ii+'.jpg');
			ii++;
		}
		
		loadText();
		function loadText(i=0){
			if (global.geometry.allSegment[i]){
				if (allImg[i]){
					let bufImg = new THREE.TextureLoader().load(
						allImg[i],
						function(){
							bufImg.minFilter = THREE.LinearFilter; 
							global.geometry.allSegment[i].material.color.setHex( 0xffffff );
							global.geometry.allSegment[i].material.opacity = 1;
							global.geometry.allSegment[i].material.map = bufImg;
							global.geometry.allSegment[i].material.needsUpdate = true;
							if (global.delSeg[i]) {global.pano.scene.remove( global.delSeg[i] );}
							i++;
							loadText(i); 
							//setTimeout(function(){loadText(i)},100);
						},
						"",
						function(){
							console.log("ERROR LOAD "+allImg[i]);
							global.geometry.allSegment[i].material.color = {r:Math.random(),g:Math.random(),b:Math.random()};
							i++;
							loadText(i); 
							//setTimeout(function(){loadText(i)},100);
						}
					);
				}
			}
			else {
				if (global.delSeg){
					let ii=0;
					while (ii <= global.delSeg.length) {
						global.pano.scene.remove( global.delSeg[ii] );
					ii++;
					}
					global.delSeg = [];
				}
			}
		}
	}
	
	function errSetImg(numb){
		if (global.delSeg){
			let ii=0;
			while (ii <= global.delSeg.length) {
				global.pano.scene.remove( global.delSeg[ii] );
			ii++;
			}
			global.delSeg = [];
		}
		for (let i = 0; i < numb ; i++){
			global.geometry.allSegment[i].material.color = {r:Math.random(),g:Math.random(),b:Math.random()};
			global.geometry.allSegment[i].material.opacity = 1;
			global.geometry.allSegment[i].material.needsUpdate = true;
		}
	}	
	
	// Создаст тагреты ( пины ) на панораме
	function CreatePins(info){
		let allT = info || false;
		if (allT && Object.keys(allT).length != 0){
			let frag = document.createDocumentFragment();
			if(!global.allTarget){global.allTarget = []}
			allT.forEach(pin => {
				let elem  = global.doc.createElement('div');
				// let t = parseInt(pin.top) || 0, l = parseInt(pin.left) || 0;
				
				if (pin.css) {elem.style.cssText = pin.css;}
				
				elem.classList.add("Hw-Three-pano-target-one");
				elem.style.position = "absolute";
				elem.style.top = "-8000px";
				elem.style.left = "-8000px";
				
				if (pin.class){
					if (pin.class.indexOf(' ') != -1){
						let arr = pin.class.split(' ');
						arr.forEach(function(item){elem.classList.add(item);});
					}
					else {elem.classList.add(pin.class);}
				}
				if (pin.height) {elem.style.height = pin.height+'px';}
				if (pin.width) {elem.style.width = pin.width+'px';}
				
				if (pin.content) {
					elem.innerHTML = pin.content;
				}
					
				if (pin.img){
					let elemImg = global.doc.createElement('img');
					let dataImg = pin.img;
					
					if(typeof dataImg == 'object'){
						elemImg.src = global.dir+'/target/'+dataImg.src;
						if (dataImg.style){elemImg.style.cssText = dataImg.style;}
					}
					
					if(typeof dataImg == 'string'){
						elemImg.src = global.dir+'/target/'+dataImg;
					}
					
					elem.appendChild(elemImg);
				}
				
				if (pin.nextPano){
					AddLisnerNextPano(elem,pin.nextPano);
					elem.dataset.panoGo = pin.nextPano;
				}
				
				if (pin.fcn){
					let func = pin.fcn;
					if (!global.TargetFcns[func]) {global.TargetFcns[func]=function(e){};}
					elem.dataset.fcn = func;
					AddLisnerTargetFunc(elem,func);
				}
				
				if (pin.data){
					elem.dataset.pin = pin.data;
				}
				

				frag.appendChild(elem);
				global.allTarget.push({e:elem,v:pin.vect});
				// global.allTarget.push({e:elem,v:pin.vect,t:t,l:l});
			});
		global.pano.panoTargetList.appendChild(frag);
		}
	}
	
	function AddLisnerNextPano(e,next){
		e.addEventListener('pointerdown',function(){object.Set(next);});
	}
	
	function AddLisnerTargetFunc(e,func){
		e.addEventListener('pointerdown',function(){
			global.TargetFcns[func](e);
		});
	}

	function Animate() {
		global.pano.renderer.render(global.pano.scene, global.pano.camera);
		PositionTarget();
		
		setTimeout( function() {
			requestAnimationFrame( Animate );
		}, 1000 / 60 );
	};

	function UnScreenPosition(){
		let vector3 = new THREE.Vector3(0,0,0.9);   

		vector3.unproject(global.pano.camera);
		
		let spriteMap = new THREE.TextureLoader().load( "img/pixel.jpg" );
		let spriteMaterial = new THREE.SpriteMaterial( { map: spriteMap, color: 0xffffff } );
		let sprite = new THREE.Sprite( spriteMaterial );
			sprite.position.set(vector1.x,vector1.y,vector1.z);
			
		let spriteMap2 = new THREE.TextureLoader().load( "img/gomer.jpg" );
		let spriteMaterial2 = new THREE.SpriteMaterial( { map: spriteMap2, color: 0xffffff } );
		let sprite2 = new THREE.Sprite( spriteMaterial2 );
			sprite2.position.set(vector3.x,vector3.y,vector3.z);
		
		global.pano.scene.add( sprite );
		global.pano.scene.add( sprite2 );
	}

	function ScreenPosition(v={}){
		let x = v.x || 0, y = v.y || 0, z = v.z || 0;
		let vector = new THREE.Vector3(x,y,z);
		vector.project(global.pano.camera);
		vector.x = ( vector.x * global.canvas.width2 ) + global.canvas.width2;
		vector.y = - ( vector.y * global.canvas.height2 ) + global.canvas.height2;
		return { 
			x: vector.x,
			y: vector.y,
			z: vector.z
		}
	};

	function PositionTarget () { // Функция котороя распологает HTML объекты отнасительно указанных векторов
		if (global.allTarget && typeof(global.allTarget) == "object"){
			global.allTarget.forEach(function (item){
				let pos = ScreenPosition(item.v);
				//console.log(item);
				let t = item.t || 0, l = item.l || 0;
				if (pos.z <= 1){
					item.e.style.top = (pos.y+t)+"px";
					item.e.style.left = (pos.x+l)+"px";
				} else {
					item.e.style.top = "-8000px";
					item.e.style.left = "-8000px";
				}
			});
		}
	}

	/* Click function */
	function CanvasClick(e){ // функция при клике на канвас
		if (!global.click) {global.click = {};}
		global.click.sx = e.pageX || e.touches[0].pageX;
		global.click.sy = e.pageY || e.touches[0].pageY;
		global.click.rotationX = global.pano.camera.rotation.y
		global.click.rotationY = global.pano.camera.rotation.x
		
		if (e.touches && e.touches.length == 2) {
			CanvasEnd();
			global.click.zoomPano = global.pano.camera.zoom;
			global.click.delta = Math.sqrt(Math.pow(e.touches[1].pageX-e.touches[0].pageX,2)+Math.pow(e.touches[1].pageY-e.touches[0].pageY,2));
			global.canvasBlock.addEventListener('touchmove',CameraZoom);
		} else {
			global.canvasBlock.addEventListener('mousemove',CanvasMove);
			global.canvasBlock.addEventListener('touchmove',CanvasMove);
			
			global.doc.addEventListener('mouseup',CanvasEnd);
			global.canvasBlock.addEventListener('touchend',CanvasEnd);
		}
	}
	// функция движения пальца или мыши по канвасу
	function CanvasMove(e){ 
		let rotationSX = global.click.rotationX;
		let rotationSY = global.click.rotationY;
		let x = e.pageX || e.touches[0]?.pageX;
		let y = e.pageY || e.touches[0]?.pageY;
		let mx = global.click.sx - x;
		let my = global.click.sy - y;
		
		let rotationX = (rotationSX-(mx*global.control.rotationSpeed)); 
		let rotationY = (rotationSY-(my*global.control.rotationSpeed)); 
		if (rotationY >= global.control.yMax) {rotationY = global.control.yMax;} 
		if (rotationY <= -global.control.yMax) {rotationY = -global.control.yMax;}
		global.pano.camera.rotation.y = rotationX;
		global.pano.camera.rotation.x = rotationY;
	}
	// увелечение панорамы 2мя пальцами
	function CameraZoom (e){ 
		let x1 = e.touches[0].pageX;
		let x2 = e.touches[1].pageX;
		let y1 = e.touches[0].pageY;
		let y2 = e.touches[1].pageY;
		
		let delta = (Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2)))-global.click.delta;
		let sz  = global.click.zoomPano+(delta*global.control.zoomTouchKof);
		
		object.SetZoom(sz);
	}
	// функция когда отпустили мыш или палец
	function CanvasEnd(){ 
		global.canvasBlock.removeEventListener('mousemove',CanvasMove);
		global.canvasBlock.removeEventListener('touchmove',CanvasMove);
		global.canvasBlock.removeEventListener('touchmove',CameraZoom);
	}

	function CameraWheel(e){ // увелечение панорамы колесиком мышки
		e.preventDefault ? e.preventDefault() : (e.returnValue = false);
		let delta = e.deltaY || e.detail || e.wheelDelta;
		let zoomPano = global.pano.camera.zoom;
		let sz  = global.pano.camera.zoom-(delta*global.control.zoomMousKof);
		object.SetZoom(sz);
	}

	/* -Click function- */
	function hw_disable_touch(elem){
		function disable_touch_event (event){
			event.preventDefault(); // Отменяет событие, если оно отменяемое
			event.stopPropagation(); // Прекращает дальнейшую передачу текущего события.
		}
		elem.addEventListener('touchstart',disable_touch_event);
		elem.addEventListener('touchmove',disable_touch_event);
		elem.addEventListener('touchend',disable_touch_event);
	}
	
	function hw_ajax (option){
		let XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
		
		let url = option.url || '';
		let type = option.type || 'GET'; 
		let convert = option.convert || '';
		let fc = option.fc || function(){};
		let err = option.err || function(){};
		
		let dataInfo = 'None date!!';

		let request = new XHR();
		request.onreadystatechange = function() {
			if(request.readyState === 4) {
				if(request.status === 200) { 
					dataInfo = request.responseText
					if (convert == 'JSON') {dataInfo = JSON.parse(dataInfo)}
					fc(dataInfo);
				} else {err();}
			}
		}
		let ver = new Date().getTime();
		request.open(type, url+"?ver="+ver);
		request.send();
	}
}