"use strict";
document.addEventListener("DOMContentLoaded", function(){
	var GD = {};
	var Pano = new Hw_Pano('#pano'); // инициализация, + указываем HTML Блок где отрисовать панараму
		// Pano.SetSelector('#pano') // Установим путь до папки, где лежат все панорамы.
		Pano.SetUrl('pano') // Установим путь до папки, где лежат все панорамы.
		Pano.Init() // Запустим панораму, отрисовку и рендер в объект
		
		// Pano.RandomColorPano() // проверочная функци, раскрасит пано в случаные цвета
		Pano.Set('pano_1') // установим панораму по имени	
		// Pano.SetPano('pano_1') // Установит ТОЛЬКО  панораму ( картинку на фоне )
		// Pano.SetPins('pano_1') // Установит ТОЛЬКО  Пины на текущею панораму ( Объекты которые отрисованы поверх панорамы )
		// Pano.SetZoom(1.5) // Установит увелечение камеры
		Pano.SetZoomFcn(zoomScrolCorrector) // Установить функцию котороя будет отрабатывать при каждом изменении зума камеры
		// Pano.SetCameraPos(0,-2.8); // Установит камеру в новую позицию
		Pano.SetFcn('clickPin_6',function(){console.log('hi i 6 pin')}); // Установит функцию реагирующею на нажатием пина
		
		
		Pano.GetCameraPos() // Полуит текущею позицию камеры
		Pano.GetZoom() // Вернёт текущий зуум пано
		Pano.GetUnScreenPosition() // Вернт текущею позицию от центра экрана в виде вектора
		Pano.GetScreenPosition() // Вернёт позицию в пикселях относительно экрана по вектору камеры ( карочи центр канвас блока ) актуалны только  X и Y
		

		// Pano.ClearPano() // Очистит всю панораму ( только фон пины останутьс )
		// Pano.ClearPins() // Очистит все пины ( только пины панорама останиться )
		
		
	
	
	let btnZoomPlus = document.querySelector('#zoom_but_plus');	
	let btnZoomMinus = document.querySelector('#zoom_but_minus');	
	let btnZoomScrol = document.querySelector('#zoom_but_scrol');
	
	let btnPano = document.querySelectorAll('.mini-but');
	
	let btnClearPins = document.querySelector('.clear_pins');
	let btnClearPano = document.querySelector('.clear_pano');
	let btnGetCamera = document.querySelector('.get_camera');
	let btnSetCamera = document.querySelector('.set_camera');
	let btnRandomColor = document.querySelector('.random_color');
		
	/* ZOOM */	
	btnZoomPlus.addEventListener("click",function(){
		GD.zoom = Pano.GetZoom()+0.5;
		Pano.SetZoom(GD.zoom);
		// zoomScrolCorrector()
	})
	btnZoomMinus.addEventListener("click",function(){
		GD.zoom = Pano.GetZoom()-0.5;
		Pano.SetZoom(GD.zoom);
		// zoomScrolCorrector()
	})
	
	
	btnPano.forEach(elem => elem.addEventListener("click",function(){
		let active = document.querySelector('.akn-w-but-rightcircle .mini-but.active');	
		let pano = this.dataset.pano;
		
		active.classList.remove('active');
		this.classList.add('active');
		Pano.Set(pano);
	}))
	
	
	btnClearPins.addEventListener("click",function(){
		Pano.ClearPins();
	})		
	btnClearPano.addEventListener("click",function(){
		Pano.ClearPano();
	})	
	
	btnGetCamera.addEventListener("click",function(){
		console.log(Pano.GetCameraPos());
	})	
	btnSetCamera.addEventListener("click",function(){
		Pano.SetCameraPos(0,-2.8);
	})	
	
	btnRandomColor.addEventListener("click",function(){
		Pano.RandomColorPano();
	})
		
	
	// корректирует отображение скрола зуммера
	function zoomScrolCorrector(){
		let zoom = Pano.GetZoom()
		btnZoomScrol.style.top = (315-((90*zoom)-45)+17)+"px"
	}	

		
	
	
	
	
	
});	






let namePano = false;	
	if (namePano){
		var Pano = new HwPano();
		var PanoCreater = new HwPanoCreater();
		var allTargetInfo = [];
		var explosion = document.querySelector('.target_boom');
		var explosionSrc = explosion.src; explosion.src = "";
		
		
		Pano.set('pano/'+namePano);
		Pano.TargetFunc.max = function(e){console.log(1111)};
		
		
		PanoCreater.hw_ajax({
			url:'pano/'+namePano+'/target/target.json',
			type:'GET',
			convert:'JSON',
			fc:function(info){
				if (info.length){
					info.forEach(function(item){
						allTargetInfo.push(item);
					});
				}
			}
		});
		
		
		var panoBlock = document.querySelector('.pano');
		var allClick = document.querySelectorAll('.click');
		var targetSet = document.querySelector('.target_settings');
		var buttonClick1 = allClick[0];
		var buttonClick2 = allClick[1];
		var buttonClick3 = allClick[2];
		var buttonClick4 = allClick[3];

		buttonClick1.addEventListener('click',function(){
			var Etop = document.querySelector('.target_top input').value || 0;
			var Eleft = document.querySelector('.target_left input').value || 0;
			var width = document.querySelector('.target_width input').value || 0;
			var height = document.querySelector('.target_height input').value || 0;
			
			var Eclass = document.querySelector('.target_class input').value || 0;
			var data = document.querySelector('.target_data input').value || 0;
			
			var css = document.querySelector('.target_css textarea').value || 0;
			var content = document.querySelector('.target_content textarea').value || 0;
			
			var imgsrc = document.querySelector('.target_img_src input').value || 0;
			var imgcss = document.querySelector('.target_img_css input').value || 0;
			
			var nextPano = document.querySelector('.target_nextPano input').value || 0;
			var func = document.querySelector('.target_func input').value || 0;

			var set = {};
				if(Etop){set.top = Etop;}
				if(Eleft){set.left = Eleft;}
				if(width){set.width = width;}
				if(height){set.height = height;}
				if(css){set.css = css;}
				if(Eclass){set.class = Eclass;}
				if(content){set.content = content;}
				if(imgsrc){set.img = {}; set.img.src = imgsrc;}
				if(imgcss){set.img.css = imgcss;}
				if(nextPano){set.nextPano = nextPano;}
				if(func){set.func = func;}
				if(data){set.data = data;}

			var vector = PanoCreater.CreateTarget(Pano,set);
				set.vect = vector;
				
			allTargetInfo.push(set);
				
				
			explosion.src = explosionSrc;
			explosion.style.display = "block";
			setTimeout(function(){explosion.style.display = "none";explosion.src = "";},(explosion.dataset.time*1000));
		});
		
		buttonClick2.addEventListener('click',function(){
			if (targetSet.style.display == "none") {targetSet.style.display = "block";}
			else {targetSet.style.display = "none";}
		});
		
		buttonClick3.addEventListener('click',function(){
			PanoCreater.hw_ajax({
				url:'download_target.php',
				type:'POST',
				info:'type=download&name='+namePano,
				fc:function(info){				
					window.location.href = "/hw-three-pano-creater/pano/"+namePano+"/"+info;
				}
			});
		});
		
		buttonClick4.addEventListener('click',function(){
			var allTarget=0;
			var jsonString = JSON.stringify(allTargetInfo);
			allTarget = 'name='+namePano+'&json='+jsonString;

			PanoCreater.hw_ajax({
				url:'save_target.php',
				type:'POST',
				info:allTarget,
				fc:function(info){
					console.log(info);
					window.location.href ='?name='+namePano;
				}
			});
		});
	}
